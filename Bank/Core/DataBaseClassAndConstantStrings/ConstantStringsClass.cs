﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Core.DataBaseClassAndConstantStrings
{
    public static class ConstantStringsClass
    {
        public const string introductionOfTheApplication = "Hello, world!" +
            " This is an application, which can be used for creation of a big institution structure!";
        public const string options = "If you want to see all services, please press \"Enter\".";
        public const string invalidCommand = "Invalid command." +
            "If you want to see all services, please press \"Enter\"." +
            " You can also enter \"Exit\" to leave the application.";
        public const string personCreated = "Person whit name {0} was created!";

    }
}
