﻿using Bank.Core.Contracts;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Bank.Core.Services
{
    public class DataBase : IDataBase
    {
        #region Fields and Properties
        private readonly List<string> commandsList;
        public DataBase()
        {
            this.commandsList = GetAllCommandNames();
        }
        public List<string> CommandsList
        {
            get
            {
                return this.commandsList;
            }
        }
        #endregion
        #region Methods
        public List<string> GetAllCommandNames()
        {
            var myAssembly = Assembly.GetExecutingAssembly();
            var commandTypes = myAssembly.DefinedTypes.Where(typeinfo => typeinfo.ImplementedInterfaces.Contains(typeof(ICommand))).ToList();
            List<string> allCommands = new List<string>();
            foreach (var item in commandTypes)
            {
                allCommands.Add(item.Name);
            }
            return allCommands;
        }
        public List<string> FilterOfCommandNames(string filter)
        {
            var allCommands = GetAllCommandNames();
            var filteredCommands = new List<string>();
            foreach (var item in allCommands)
            {
                if (item.Contains(filter))
                {
                    filteredCommands.Add(item);
                }
            }
            return filteredCommands;
        }
        #endregion

    }
}
