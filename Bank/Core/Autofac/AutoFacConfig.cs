﻿using Autofac;
using System.Reflection;
using System;
using Bank.Core.Contracts;
using System.Linq;

namespace Bank.Core.Autofac
{
    public class AutoFacConfig 
    {
        public IContainer Build()
        {
            var builder = new ContainerBuilder();
            return builder.Build();
            
        }
        public void RegisterConvention(ContainerBuilder builder)
        {
            var myAssembly = Assembly.GetExecutingAssembly();
            builder.RegisterAssemblyTypes(myAssembly).AsImplementedInterfaces();

        }
        public void RegisterCoreComponents(ContainerBuilder builder)
        {
            var myAssembly = Assembly.GetExecutingAssembly();
            var commandTypes = myAssembly.DefinedTypes.Where(typeInfo => 
            typeInfo.ImplementedInterfaces.Contains(typeof(ICommand))).ToList();
            foreach (var commandType in commandTypes)
            {
                builder.RegisterType(commandType.AsType()).Named<ICommand>(commandType.Name.ToLower());
            }



            
            //builder.RegisterType<Engine>().As<IEngine>().SingleInstance();
            //builder.RegisterType<VehicleFactory>().As<IVehicleFactory>().SingleInstance();
            //builder.RegisterType<JourneyFactory>().As<IJourneyFactory>().SingleInstance();
            //builder.RegisterType<TicketFactory>().As<ITicketFactory>().SingleInstance();
            //builder.RegisterType<DataBase>().As<IDataBase>().SingleInstance();
        }
    }
}









