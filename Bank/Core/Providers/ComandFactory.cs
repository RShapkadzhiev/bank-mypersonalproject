﻿using Autofac;
using Bank.Core.Contracts;

namespace Bank.Core.Providers
{
    public class ComandFactory : ICommandFactory
    {
        private readonly IComponentContext contex;
        public ComandFactory(IComponentContext contex)
        {
            this.contex = contex;
        }
        public ICommand GetTheCommand(string commandName)

        {
            return this.contex.ResolveNamed<ICommand>(commandName);
        }
    }
}
