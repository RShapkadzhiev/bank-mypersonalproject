﻿using Bank.Core.Contracts;
using System;
using System.Collections.Generic;

namespace Bank.Core.Providers
{
    public class ConsoleWriter : IConsoleWriter
    {
        public void WriteSingleLine(string message)
        {
           Console.WriteLine(message);
        }
        public void WriteList(List<string> myList)
        {
            foreach (var item in myList)
            {
                Console.WriteLine(item);
            }
        }
    }
}
