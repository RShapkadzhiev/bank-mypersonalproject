﻿using Bank.Core.Contracts;
using System;

namespace Bank.Core.Providers
{
    public class ConsoleReader : IConsoleReader
    {
        public string Input()
        {
            return Console.ReadLine();
        }
    }
}
