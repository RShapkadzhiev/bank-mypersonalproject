﻿using Bank.Core.Contracts;
using Bank.Core.DataBaseClassAndConstantStrings;
using System.Collections.Generic;

namespace Bank.Core
{
    public class Engine
    {
        #region Fields
        private readonly IConsoleReader reader;
        private readonly ICommandParser parser;
        private readonly IDataBase dataBase;
        private readonly IConsoleWriter writer;
        private readonly ICommandFactory factory; 
        #endregion
        #region Constructor
        public Engine(IConsoleReader reader, ICommandParser parser, IDataBase dataBase,
            IConsoleWriter writer, ICommandFactory factory)
        {
            this.reader = reader;
            this.parser = parser;
            this.dataBase = dataBase;
            this.writer = writer;
            this.factory = factory;
        }
        #endregion
        #region Methods
        public void Start()
        {
            do
            {
                writer.WriteSingleLine(ConstantStringsClass.introductionOfTheApplication);
                writer.WriteSingleLine(ConstantStringsClass.options);
                var input = reader.Input();
                List<string> myList = parser.ParseTheInput(input);
                if (myList.Count == 0)
                {
                    writer.WriteList(dataBase.GetAllCommandNames());
                }
                if (myList.Count >= 1)
                {
                    writer.WriteSingleLine(ConstantStringsClass.invalidCommand);
                }
                else
                {
                    var commandName = myList[0];
                    int commandInt;
                    if (int.TryParse(commandName, out commandInt))
                    {
                        var intToCommand = dataBase.GetAllCommandNames();
                        commandName = intToCommand[commandInt - 1];
                    }
                    var command = this.factory.GetTheCommand(commandName);
                    writer.WriteSingleLine(command.Execute());
                }
            }
            while (true);
            
        }
        #endregion
    }
}
