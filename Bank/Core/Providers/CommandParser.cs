﻿using Bank.Core.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace Bank.Core
{
    public class CommandParser :ICommandParser
    {
        char delimiter = ' ';
        List<string> myList = new List<string>();

        public List<string> ParseTheInput(string commandAndParameters)
        {
            myList = commandAndParameters.Trim().Split(delimiter).ToList();
            return myList;
        }
    }
}
