﻿using System.Collections.Generic;

namespace Bank.Core.Contracts
{
    public interface ICommand
    {
        string Execute();
    }
}
