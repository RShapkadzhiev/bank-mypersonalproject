﻿using System.Collections.Generic;
using Bank.Core.Contracts;
using Bank.Core.DataBaseClassAndConstantStrings;

namespace Bank.Core.Services
{
    public class CreatePerson : ICommand
    {
        public string Execute()
        {
            return string.Format(ConstantStringsClass.personCreated, person.Name);
        }

     
    }
}
