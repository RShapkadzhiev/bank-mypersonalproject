﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Core.Contracts
{
    public interface ICommandParser
    {
        List<string> ParseTheInput(string commandAndParameters);
    }
}
