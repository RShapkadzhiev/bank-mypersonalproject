﻿namespace Bank.Core.Contracts
{
    public interface ICommandFactory
    {
        ICommand GetTheCommand(string commandName);
    }
}
