﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Core.Contracts
{
    public interface IDataBase
    {
        List<string> GetAllCommandNames();
        List<string> FilterOfCommandNames(string filter);
    }
}
