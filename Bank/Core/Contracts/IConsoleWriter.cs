﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Core.Contracts
{
    public interface IConsoleWriter
    {
        void WriteSingleLine(string message);
        void WriteList(List<string> myList);
    }
}
