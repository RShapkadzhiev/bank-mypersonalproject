﻿using Autofac;

namespace Bank.Core.Contracts
{
    public class IGetCommandNamer
    {
        private readonly IComponentContext context;

        public IGetCommandNamer(IComponentContext context)
        {
            this.context = context;
        }
        public ICommand ParseCommand(string commandName)
        {
            var command = this.context.ResolveNamed<ICommand>(commandName.ToLower());
            return command;
        }

    }
}
