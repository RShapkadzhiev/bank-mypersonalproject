﻿using Bank.ValidationClasses;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Employees
{
    public abstract class Persons
    {
        #region Fields
        string firstName;
        string middleName;
        string lastName;
        string crsNumber;
        string bankAccount;
        ulong egn;
        #endregion
        #region Constructors
        //public Persons(string firstName, string middleName, string lastName)
        //{
        //    this.FirstName = firstName;
        //    this.MiddleName = middleName;
        //    this.LastName = lastName;
        //}
        //public Persons(string firstName, string middleName, string lastName, string crsNumber, string bankAccount, ulong egn)
        //    :this(firstName,middleName,lastName)
        //{
        //    this.CrsNumber = crsNumber;
        //    this.BankAccount = bankAccount;
        //    this.Egn = egn;
        //}
        #endregion
        #region Properties
        public string FirstName
        {
            get
            {
                return this.firstName;
            }
            set
            {
                if (CheckForStringLenght.IsLengthRigth(value, 3, 20))
                {
                    this.firstName = value;
                }
                else
                {
                    //Това следва да го изведа в отделен метод и да му подам в интерполиран стринг нещо което да взема обекта и да казва следното:
                    //The description of the story cant be smaller than 100 and bigger than 200 characters.
                    //В друг случай да казва - The name of the Member can be smaller than 3 and bigger than 20 characters.
                    Console.WriteLine($"Invalid name! Please enter a {this.GetType().Name[0].ToString()} which is longer than 3 characters and smaller than 20 characters");
                    this.firstName = Console.ReadLine();
                }
            }
        }
        #endregion
        #region Methods
        #endregion

    }
}
