﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Models.Persons
{
    public abstract class Person
    {
        #region Fields
        private readonly string firstName;
        private readonly string middleName;
        private readonly string lastName;
        private readonly ulong egn;
        private readonly sbyte age;
        private readonly string adress;
        private readonly string phoneNumber;
        private readonly Guid id;
        #endregion
        #region Constructor
        public Person(ulong egn)
        {
            this.egn = egn;
        }
        //jfnkf
        public Person()
        {

        }
        #endregion
    }
}
