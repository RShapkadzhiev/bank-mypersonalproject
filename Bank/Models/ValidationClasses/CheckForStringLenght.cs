﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.ValidationClasses
{
    class CheckForStringLenght
    {
        public static bool IsLengthRigth(string input, int minLenght, int maxLenght)
        {
            if (!CheckNullString.IsStringNullEmptyOrWhitespace(input))
            {
                if (input.Length < minLenght || input.Length > maxLenght)
                {
                    return false;
                }
                return true;
                
            }
            return false;
        }
    }
}
