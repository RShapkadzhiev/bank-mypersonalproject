﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.ValidationClasses
{
    class CheckNullString
    {
        #region Methods
        public static bool IsStringNullEmptyOrWhitespace(string input)
        {
            if (input.Contains(" "))
            {
                input = input.Replace(" ", "");
            }
            if (string.IsNullOrEmpty(input))
            {
                return true;
            }
            return false;
        }
        #endregion
    }
}
